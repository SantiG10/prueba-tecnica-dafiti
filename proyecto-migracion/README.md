# Guia para la migración del proyecto para Kubernetes

**Requisitos previos para levantar la aplicación**

- Ambiente de test de kubernetes “minikube”, “mini k8s”, "Kubernetes Docker Desktop" o servicio de kubernetes en la nube configurado y con las credenciales para su conexión.
- Servicio de RabbitMQ corriendo, en este caso vamos a instalar RabbitMQ en nuestro propio kubernetes.
- Base de datos Postgres corriendo y funcionando.
- Las imagenes de docker de los servicios api y listener construidas y en algun container registry 

**NOTA:** 

- En mi caso utilizare Kubernetes Docker Desktop.
- Los servicios RabbitMQ y Postgres se podrian usar con un proveedor de nube como AWS, Azure, GPC, en mi caso los ejecutare dentro de k8s.
- La imagenes de docker de los servicios la almacenare en mi registry local.

#### IMPORTANTE

Se recomienda seguir la guía paso a paso para su entendimiento completo.

### **1. Creacion de namespaces**

Para la creacion de los namespaces ejecutamos:
```
kubectl apply -f k8s/01-namespaces.yml
```

Esto nos creara dos namespaces, migration-project donde se creara todos los componentes del proyecto a migrar y el namespace rabbitmq donde se instalara el operador de rabbitmq con la ayuda helm.

### **2. Instalación de operador de cluster de RabbitMQ para Kubernetes**

Para la Instalación del operador de RabbitMQ vamos a utilizar el manejador de paquetes de Kubernetes Helm.

Lo primero que debemos hacer es agregar el repositorio con helm
```
helm repo add bitnami https://charts.bitnami.com/bitnami
```
Instalamos el operador con helm, si queremos le damos un namespace diferente al default. en este caso se va llamar rabbitmq
```
helm install rabbitmq bitnami/rabbitmq-cluster-operator  --namespace rabbitmq
```

**3. Instalación del cluster de RabbitMQ**

Para la creación del clusterde RabbitMQ ejecutamos
```
kubectl apply -f k8s/02-rabbitmq.yml 
```
Para visualizar si el cluster se creo correctamente ejecutamos
```
kubectl get all -n migration-project
```
Se deben visualizar los pods y services creados con exito y corriendo

### **4. Despliegue base de datos Postgres**

Para el Despliegue de la base de datos se deben crear componentes adicionales para persistir los datos, esto es muy importante dado que si no se crean estos volumenes persistentes podria pasar un accidente y perder los datos de la base de datos.

- En primer lugar debemos crear un volumen persistente, para eso ejecutamos
```
kubectl apply -f k8s/03-persistent-volume.yml
```
Para verififcar la creacion
```
kubectl get pv
```

-  Luego creamos un Persisten Volume Claim, este nos ayuda a enlazar nuestra base de datos (pod) con el volumen persistente creado anteriormente.
```
kubectl apply -f k8s/04-persistent-volume-claim.yml
```
Para verififcar la creacion
```
kubectl get pvc -n migration-project
```
- Por ultimo creamos nuestro deployment y service de Postgres

Para ejecutar el deployment 
```
kubectl apply -f k8s/05-deployment-db-postgresql.yml
```
Nos creara un pod con la imagen de **postgres** y enlaza el volumen recien creado.

**NOTA:** _Para esta ocasión a modo testing se quemaron las credenciales de la base de datos (user, pass) en el codigo fuente. esto en ninguna ocasión es recomendable para un ambiente de producción, se debe utilizar secret manager o alguna otra herramienta similar para encriptar esta información._

Para la creacion del servcice 
```
kubectl apply -f k8s/06-service-db-postgresq.yml
```
Podemos validar todos los componentes creados ejecutando
```
kubectl get all -n migration-project
```

### **5. Construcción imagenes Docker**

Para construir las imagenes de los servicios y poder referenciarlas en nuestro manifiesto de deployment en kubernetes debemos crearlas con el comando `docker build` entrando a cada una de las carpetas `api` y `listener`

**- Carpeta listener**

Antes de construir la imagen del servicio listener debemos hacer una modificación de los HOST de los servicios RabbitMQ y Postgres en el codigo fuente.

Los valores van a ser los nombres de los services `ClusterIP` que creamos de tipo  anteriormente dado que el DNS interno de kubernetes nos ayuda a resolver al servicio correcto (pod), cabe aclarar que esto es para este ambiente de testing.

Podemos visualizar los nombres de los servicios con el momando 
```
kubectl get svc -n migration-project
```
- En la linea `6` cambiamos `DB_HOST = 'db'` por `DB_HOST = 'postgresql'` en el archivo `db.py`
- En la linea `14` cambiamos `rabbitmq` por `rabbitmqcluster` en el archivo `main.py`

**NOTA:** El manejo de estas variables es recomendable hacerlo por mediante una herramienta de secret manager.

Después de hacer la modificación si podemos hacer el build de la imagen con el comando
```
docker build . -t dafiti/listener
```

**- Carpeta api**

Primero cambiamos el nombre sel service 

- En la linea `12` cambiamos `rabbitmq` por `rabbitmqcluster` en el archivo `main.py`

```
docker build . -t dafiti/api
```

Ya con esto tenemos nuestras imagenes de docker construidas y almacenadas en nuestro registry local.

**IMPORTANTE:** si no se crean las imagenes con estos nombres `dafiti/api` y `dafiti/listener` debera cambiarlos en el manifiesto de los deployments por los nombres que les dio.

### **6. Despliegue de Listener**

Para la creación del deployment del servicio listener ejecutamos
```
kubectl apply -f k8s/07-deployment-listener.yml
```
puedes ver si el pod esta corriendo exitosamente con
```
kubectl get pod -n migration-project
```
Si por alguna razón hay un error o quieres ver el log del servicio puedes ejecutar
```
kubectl logs <name-pod> -n migration-project
```
Debes de visualizar un mensaje en el log si todo esta funcionando perfectamente
```
creando las tablas
 [*] Waiting for messages. To exit press CTRL+C
```

### **7. Despliegue del Api**

Para la creación del deployment del servicio listener ejecutamos
```
kubectl apply -f k8s/08-deployment-api.yml 
```
puedes ver si el pod esta corriendo exitosamente con
```
kubectl get pod -n migration-project
```
Si por alguna razón hay un error o quieres ver el log del servicio puedes ejecutar
```
kubectl logs <name-pod> -n migration-project
kubectl logs api-6d55bc56d8-2fbf5 -n migration-project #example
```
Debes de visualizar un mensaje en el log si todo esta funcionando perfectamente
```
INFO:     Will watch for changes in these directories: ['/app']
INFO:     Uvicorn running on http://0.0.0.0:80 (Press CTRL+C to quit)
INFO:     Started reloader process [8] using watchgod
INFO:     Started server process [14]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
```

**Service NodePort Api**

Para el servicio API vamos a utilizar el tipo NodePort para poder acceder al servicio desde un ambiente local.

Ejecutamos el el siguiente comando para crearlo
```
kubectl apply -f k8s/09-service-api.yml 
```

## Testear la aplicación

Para realizar pruebas de la aplicación basta con correr en el navegador la url `http://localhost:30000/`

O bien ejeuctar en una terminal el comando `curl localhost:30000`

Podemos darle seguimiento a los logs de listener con el siguiente comando

```
kubectl logs <name-pod> -n migration-project -f
kubectl logs listener-5c775645dd-6z58h -n migration-project -f #example
```

Debemos esperar estos mensajes en el log si todo esta funcionando bien

```
ejecutando tarea pesada
tarea lista
 [x] Received b'Hello World!'
ejecutando tarea pesada
tarea lista
 [x] Received b'Hello World!'
ejecutando tarea pesada
tarea lista
```

También podemos verififcar si se estan inyectando los mensajes en la base de datos. para ello podemos entrar al pod de la base de datos con el comando
```
kubectl exec -n migration-project -it <name-pod-postgres> --  psql -h localhost -U devops --password devops -p 5432 postgresdb

#example
kubectl exec -n migration-project -it postgresql-bb864978d-llp7z --  psql -h localhost -U devops --password devops -p 5432 postgresdb
```
Te debe pedir la password la cual es `devops`

Una vez adentro puedes consultar los registros de la tabla mensaje con el Query `SELECT * FROM mensaje;`

Debes de visualizar algo como:
```
 id |   mensaje
----+--------------
  1 | Hello World!
  2 | Hello World!
  3 | Hello World!
  4 | Hello World!
  5 | Hello World!
  6 | Hello World!
  7 | Hello World!
(7 rows)
```

## Mejoras de Rendimiento de los servicios

Para mejorar la disponibilidad y escabilidad de los servicios vamos a utilizar HPA (Horizontal Pod Autoscaling) en los servicios API y Listener

Primero debemos instalar el controlador metrics para que funcione HPA
```
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```

Para que funcione en `Kubernetes docker Desktop` hay que ejecutar un comando para que funcione sin certificados firmados de los nodos, esto obviamente por ser un ambiente local (testing).

```
kubectl -n kube-system patch deployment metrics-server --type=json \
-p='[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--kubelet-insecure-tls"}]]'
```

Una vez teniendo configurado las metricas podemos crear los HPA para los servicios

```
kubectl apply -f k8s/10-hpa-api.yml   
kubectl apply -f k8s/11-hpa-listener.yml
```

Con esto ya tenemos configuado el HPA para los servicios, para esta prueba utilizamos el factor de la utilización de `CPU` como mecanismo de escalar los servicios si el porcentaje de CPU supera el `50%`.

Si desea encontrar algunas capturas de pantalla de los test realizados y la prueba de performance y el escalamiento de la api puede encontrarla en la carpeta `docs-img`
